package drishti.uiautomation.uiautomation;

import static org.testng.Assert.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class AppTest {
	@Test()
	public void testLoginUrl() {

		try {

			System.setProperty("webdriver.chrome.driver",
					"C:\\Users\\Certification\\eclipse-workspace\\uiautomation\\src\\main\\java\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.get("https://qa.ameyo.com:8443/app");
			String title = driver.getTitle();
			assertEquals(title, "Ameyo");
			driver.findElement(By.xpath("//input[contains(@class,'select-dropdown') and @type=\"text\"]")).click();
			List<WebElement> langlist = driver.findElements(By.xpath("//ul//li//span[contains(text(),'')]"));
			Thread.sleep(5000);

			for (WebElement w : langlist) {
				System.out.println(w.getText());
			}
			langlist.get(0).click();
			Thread.sleep(5000);

			driver.findElement(By.xpath("//input[@type=\"text\" and contains(@placeholder,'User')]")).sendKeys("QA");
			driver.findElement(By.xpath("//input[@type=\"password\" and contains(@placeholder,'Password')]"))
					.sendKeys("QA");
			driver.findElement(By.xpath("//span[contains(text(),'Login')]//ancestor::button")).click();
			// driver.close();
			if (driver.findElement(By.xpath("//span[contains(text(),'Ok')]//ancestor::button[@id='automationButton1']"))
					.isDisplayed()) {
				driver.findElement(By.xpath("//span[contains(text(),'Ok')]//ancestor::button[@id='automationButton1']"))
						.click();
			}
			driver.findElement(By.xpath("//input[@type='text' and contains(@value,'Select Ticket Campaign')]")).click();
			driver.findElement(By.xpath("//input[@type='text' and contains(@value,'Select Voice Campaign')]")).click();
			driver.findElement(By.xpath("//input[@type='text' and contains(@value,'Select Chat Campaign')]")).click();
			Alert alert = driver.switchTo().alert();
			alert.accept();
			String campaignUrl = "agentConfiguration";
			assertEquals(driver.getCurrentUrl().contains(campaignUrl), true);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
